```rs
pub struct Iter;static mut COUNT: u8 = 0;
impl Iterator for Iter { type Item = u8;
    fn next(&mut self) -> Option<Self::Item> { unsafe {
            match COUNT {
                0..=15 => {COUNT += 1; Some(COUNT)
                } _ => None, }}}
}
fn main() {  Iter.for_each(|_| println! {"Hello, World"})}
```

<!-- ![goldennight](/uploads/8019253590c55382fae28430dac9ee4b/goldennight.gif) -->